# Mini Regex Challenge

## About

A Lecturer @ The National Software Academy wanted a regular expression that could match any string that meets the requirements listed below.

The reward: sweet

## Requirements

string >= 1 digit

string >= 1 underscore

## The regular expression

I had several versions and attempts, some closer than others.
`\w*\d+\w*_+\w*|\w*_+\w*\d\w*` was my first close attempt but it didn't match special characters.

Then I decided that `[^\s]` (Not whitespace) would achieve what I wanted instead of `\w`
with the result being: `[^\s]*\d+[^\s]*_+[^\s]*|[^\s]*_+[^\s]*\d+[^\s]*`

Which I shortened to: `[^\s]*(\d+[^\s]*_+|_+[^\s]*\d+)+[^\s]*`

One flaw I noticed is that you cannot have spaces in the string.

Note: I do realise that I could have done something like `[\w\W]` rather than `[^\s]` however this causes issues with carriage returns.

Then I realised `.` accepts anything but ignores new lines

My next version was: `.*(\d+.*_+|_+.*\d+)+.*`

### The Final Version

`.*(\d.*_|_.*\d).*`
